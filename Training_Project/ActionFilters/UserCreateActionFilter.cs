﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Training_Project.ActionFilters
{
    public class UserCreateActionFilter : ActionFilterAttribute
    {
        private readonly ILogger _logger;
        public UserCreateActionFilter(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger("CustomActionFilter");
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            _logger.LogInformation("Inside OnActionExecuting method...");
            base.OnActionExecuting(context);
        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            _logger.LogWarning("Inside OnActionExecuted method...");
            base.OnActionExecuted(context);
        }

    }
}

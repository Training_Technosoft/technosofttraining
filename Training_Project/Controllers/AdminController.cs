﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Training_Project.ActionFilters;
using Training_Project.Models;
using Training_Project.Repository;
using Training_Project.Services;
using Training_Project.ViewModel;



namespace Training_Project.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManeger;
        private readonly RoleManager<IdentityRole> _roleManeger;
        private readonly IEmailSender _emailSender;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly INotificationSender _notificationSender;
        private readonly IMessageRepository _messageRepository;

        public AdminController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager,
            IEmailSender emailSender, IDepartmentRepository departmentRepository, INotificationSender notificationSender,
            IMessageRepository messageRepository)
        {
            _userManeger = userManager;
            _roleManeger = roleManager;
            _emailSender = emailSender;
            _departmentRepository = departmentRepository;
            _notificationSender = notificationSender;
            _messageRepository = messageRepository;
        }

       
        [HttpGet]
        [ServiceFilter(typeof(UserCreateActionFilter))]
        public IActionResult AddUser()
        {
            UserInputViewModel userInput = new UserInputViewModel();
            userInput.Roles = _roleManeger.Roles.ToList();
            userInput.Departments = _departmentRepository.GetAll();
            return View(userInput);
        }
        [HttpPost]
        public async Task<IActionResult> AddUser(UserInputViewModel userInput)
        {

            ApplicationUser User = new ApplicationUser();
            User.UserName = userInput.UserName;
            User.Email = userInput.Email;
            User.DepartmentId = userInput.Department;
            string password = StringGenerator.RandomPasswordGenerator(10);
            var previousUser=await _userManeger.FindByEmailAsync(userInput.Email);
            bool isEmailPresent = previousUser == null ? false : true;
            IdentityResult result=null;
            if(!isEmailPresent)
                result = await _userManeger.CreateAsync(User, password);
            if (!isEmailPresent &&  result.Succeeded)
            {
                var code = await _userManeger.GenerateEmailConfirmationTokenAsync(User);
                code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                var callbackUrl = Url.Page(
                    "/Account/ConfirmEmail",
                    pageHandler: null,
                    values: new { area = "Identity", userId = User.Id, code = code },
                    protocol: Request.Scheme);

                await _emailSender.SendEmailAsync(User.Email, "Confirm your email",
                    $"<h2>Default Credentials</h2><br/><b>Email : {User.Email}</b><br/><b>Password : {password}</b><br/> Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
                IdentityRole role = _roleManeger.Roles.Where(m => m.Id == userInput.Role).FirstOrDefault();
                await _userManeger.AddToRoleAsync(User, role.Name);
                return RedirectToAction("Index", "Home");
            }
            if (result == null)
            {
                ModelState.AddModelError(string.Empty, userInput.Email + " email already exists.");
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }
            UserInputViewModel UserRole = new UserInputViewModel();
            UserRole.Roles = _roleManeger.Roles.ToList();
            UserRole.Departments = _departmentRepository.GetAll();
            return View(UserRole);
        }

        public async Task<IActionResult> ChangeUserStatus(string id,bool status)
        {
            var user =await _userManeger.FindByIdAsync(id);
            user.isBlock= status;
            await _userManeger.UpdateAsync(user);
            return RedirectToAction("Details", "User", new { email =  user.Email});
        }

        public async Task<IActionResult> BestEmployee(DepartmentUsers departmentUsers = null)
        {

            if(departmentUsers!=null && departmentUsers.UserId!=null)
            {
                var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
                var chosenUser = await _userManeger.FindByIdAsync(departmentUsers.UserId);
                Message message = new Message()
                {
                    MessageBody = chosenUser.UserName +" is the Best employee of "+ departmentUsers.DepartmentId +" Department",
                    SenderId = loggedInUser.Id,
                    Type = "Notification",
                    SendingTime = DateTime.Now
                };
                _messageRepository.Create(message);
                var users=_userManeger.Users.Where(m => m.Id != loggedInUser.Id && m.EmailConfirmed == true).ToList();
                foreach(var user in users)
                {
                    _notificationSender.SendNotificationToUser("", user.Id, "", message);
                }
                return RedirectToAction("Details", "User", new { email = chosenUser.Email });
            }
           departmentUsers.Departments = _departmentRepository.GetAll().ToList();
           if (departmentUsers.DepartmentId != null)
           {
               departmentUsers.Users = _userManeger.Users.Where(m => m.DepartmentId == departmentUsers.DepartmentId).ToList();
                departmentUsers.DepartmentId = _departmentRepository.GetById(departmentUsers.DepartmentId).DepartmentName;
            }
            return View(departmentUsers);
        }
    }
}

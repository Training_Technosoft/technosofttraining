﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Training_Project.Hubs;
using Training_Project.Models;
using Training_Project.Repository;
using Training_Project.ViewModel;

namespace Training_Project.Controllers
{
    public class DepartmentController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManeger;
        private readonly IDepartmentRepository _departmentRepository;
        public DepartmentController(UserManager<ApplicationUser> userManager, IDepartmentRepository departmentRepository)
        {
            _userManeger = userManager;
            _departmentRepository = departmentRepository;
        }

        [Authorize(Roles ="Admin")]
        public IActionResult Create()
        {
            return View();
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Department department)
        {
            bool isPresent = _departmentRepository.isDepartmentPresent(department.DepartmentName);
            if (isPresent)
            {
                ModelState.AddModelError(string.Empty, "Department name " + department.DepartmentName + " already exists");
                return View();
            }
            _departmentRepository.Create(department);
            return RedirectToAction("List", "Department");
        }
        [Authorize(Roles = "Admin")]
        public IActionResult List(string search)
        {
            List<Department> departments;
            if (String.IsNullOrEmpty(search))
                departments = _departmentRepository.GetAll().ToList();
            else
                departments = _departmentRepository.GetAll().Where(m => m.DepartmentName.Contains(search)).ToList();
            return View(departments);
        }
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(string id)
        {
            if (String.IsNullOrEmpty(id))
                return NotFound();
            Department department = _departmentRepository.GetById(id);
            return View(department);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Department department)
        {
            if (_departmentRepository.Update(department))
                return RedirectToAction("List", "Department");
            ModelState.AddModelError(string.Empty, "Department name " + department.DepartmentName + " already exists");
            return View();
        }
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(string id)
        {
            if (String.IsNullOrEmpty(id))
                return NotFound();
            _departmentRepository.Delete(id);
            return RedirectToAction("List", "Department");
        }


        [Authorize]
        public async Task<IActionResult> Details(string id)
        {
            if (!User.IsInRole("Admin"))
            {
                var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
                if (loggedInUser.DepartmentId != id)
                    return RedirectToAction("Index", "Home");
            }
            var department = _departmentRepository.GetById(id);
            department.Users = _userManeger.Users.Where(m => m.DepartmentId == id && m.EmailConfirmed == true).ToList();
            return View(department);
        }

    }
}

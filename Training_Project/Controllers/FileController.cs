﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Training_Project.Data;
using Training_Project.Models;

namespace Training_Project.Controllers
{
    [Authorize]
    public class FileController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManeger;
        private readonly IWebHostEnvironment _hostingEnv;
        private readonly ApplicationDbContext _dbContext;
        public FileController(UserManager<ApplicationUser> userManager, IWebHostEnvironment hostingEnv,ApplicationDbContext dbContext)
        {
            _hostingEnv = hostingEnv;
            _dbContext = dbContext;
            _userManeger = userManager;
        }
  
        public IActionResult UploadFile()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null)
            {
                ModelState.AddModelError(String.Empty, "Choose a pdf File");
                return View();
            }
            if (file.ContentType != "application/pdf")
            {
                ModelState.AddModelError(String.Empty, "You can only upload pdf File");
                return View();
            }
            var user = await _userManeger.GetUserAsync(HttpContext.User);
            string UserDirectory = Path.Combine(_hostingEnv.WebRootPath, "UserFiles", user.Id);
            if (!Directory.Exists(UserDirectory))
            {
                Directory.CreateDirectory(UserDirectory);
            }
            //Random File Name
            DateTime dateTime = DateTime.Now;
            string fileName = dateTime.ToFileTime().ToString();
            fileName += file.FileName;
            string URL = Path.Combine(UserDirectory, fileName);
            using (FileStream fileStream = new FileStream(URL, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
            FileUser fileUser = new FileUser(file.FileName, URL, user.Id);
            _dbContext.Files.Add(fileUser);
            _dbContext.SaveChanges();
            return RedirectToAction("List","File");
        }

        public async Task<IActionResult> List(string search = null)
        {
            var user = await _userManeger.GetUserAsync(HttpContext.User);
            var Files = _dbContext.Files.Where(option => option.UserId == user.Id);
            if (search != null)
                Files = Files.Where(m => m.FileName.Contains(search));
            return View(Files);
        }
        [HttpPost]
        public async Task<IActionResult> UploadProfilePicture(IFormFile file)
        {
            if (file == null)
            {
                return RedirectToAction("MyProfile", "Home");
            }
            if (file.ContentType.ToLower() != "image/jpg" &&
                   file.ContentType.ToLower() != "image/jpeg" &&
                   file.ContentType.ToLower() != "image/pjpeg" &&
                   file.ContentType.ToLower() != "image/gif" &&
                   file.ContentType.ToLower() != "image/x-png" &&
                   file.ContentType.ToLower() != "image/png")
            {
                return RedirectToAction("MyProfile", "Home");
            }
            var user = await _userManeger.GetUserAsync(HttpContext.User);
            if (user.ProfilePicturePath != null)
            {
                System.IO.File.Delete(user.ProfilePicturePath);
            }
            string UserDirectory = Path.Combine(_hostingEnv.WebRootPath, "UserImages");
            string fileName = user.Id + file.FileName;
            string URL = Path.Combine(UserDirectory, fileName);
            using (FileStream fileStream = new FileStream(URL, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }
            user.ProfilePicturePath = URL;
            await _userManeger.UpdateAsync(user);
            return RedirectToAction("MyProfile", "User");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting.Internal;
using Training_Project.Data;
using Training_Project.Models;
using Training_Project.ViewModel;
using Training_Project.Services;
using Training_Project.Repository;
using Microsoft.AspNetCore.Authorization;

namespace Training_Project.Controllers
{
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            if (User.IsInRole("Admin"))
                return RedirectToAction("List", "User");
            if(User.Identity.IsAuthenticated)
                return RedirectToAction("Tasks", "User");
            return View();           
        }        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Training_Project.Models;
using Training_Project.ViewModel;

namespace Training_Project.Controllers
{
    [Authorize(Roles ="Admin")]
    public class RoleController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManeger;
        private readonly RoleManager<IdentityRole> _roleManeger;
        public RoleController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManeger = userManager;
            _roleManeger = roleManager;
        }
        
        public IActionResult List(string search = null)
        {
            List<IdentityRole> roleList;
            if (String.IsNullOrEmpty(search))
                roleList = _roleManeger.Roles.ToList();
            else
                roleList = _roleManeger.Roles.Where(role => role.Name.Contains(search)).ToList();
            return View(roleList);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(IdentityRole roleInput)
        {
            if (ModelState.IsValid)
            {
                bool isRolePresent = await _roleManeger.RoleExistsAsync(roleInput.Name);
                if (!isRolePresent)
                {
                    await _roleManeger.CreateAsync(roleInput);
                    return RedirectToAction("List", "Role");
                }
                ModelState.AddModelError(String.Empty, $"{roleInput.Name } named role already exists");
                return View();
            }
            return View();
        }
        public async Task<IActionResult> Details(string Id)
        {
            var role = await _roleManeger.FindByIdAsync(Id);
            var users = await _userManeger.GetUsersInRoleAsync(role.Name);

            RoleUsersViewModel roleUsers = new RoleUsersViewModel()
            {
                Role = role,
                Users = users.ToList()
            };
            return View(roleUsers);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string Id)
        {
            var role = await _roleManeger.FindByIdAsync(Id);
            if (role == null)
                return NotFound();

            var result = await _roleManeger.DeleteAsync(role);

            if (result.Succeeded)
                return RedirectToAction("List", "Role");
            //Internal Server Error
            return StatusCode(500);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Training_Project.Models;
using Training_Project.Repository;
using Training_Project.ViewModel;

namespace Training_Project.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskRepository _taskRepository;
        private readonly UserManager<ApplicationUser> _userManeger;
        public TaskController(ITaskRepository taskRepository, UserManager<ApplicationUser> userManager)
        {
            _taskRepository = taskRepository;
            _userManeger = userManager;
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> List(string search = null, string UserId = null)
        {
            UserTasks userTask = new UserTasks();
            if (UserId == null)
                userTask.tasks = _taskRepository.GetAll().ToList();
            else
            {
                userTask.tasks = _taskRepository.GetUserTask(UserId).ToList();
                userTask.user = await _userManeger.FindByIdAsync(UserId);
            }
            if (!String.IsNullOrEmpty(search))
                userTask.tasks = userTask.tasks.Where(task => task.Type.Contains(search) || task.Description.Contains(search)).ToList();
            return View(userTask);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult Create(Models.Task task)
        {
            if (ModelState.IsValid)
            {
                if(!_taskRepository.Create(task))
                {
                    ModelState.AddModelError(string.Empty, "Duplicate Task");
                    return View();
                }
                return RedirectToAction("List", "Task");
            }
            ModelState.AddModelError(string.Empty, "Error creating a Task");
            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Edit(string id)
        {
            if(String.IsNullOrEmpty(id))
                return NotFound();
            Models.Task task = _taskRepository.GetById(id);
            if (task == null)
                return NotFound();
            return View(task);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Models.Task task)
        {
            if(ModelState.IsValid)
            {
                if(!_taskRepository.Update(task))
                {
                    ModelState.AddModelError(string.Empty, "Duplicate Task");
                    return View();
                }
                return RedirectToAction("List", "Task");
            }
            return BadRequest();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(string id)
        {
            if(_taskRepository.Delete(id))
                 return RedirectToAction("List", "Task");
            return NotFound();
        }

        [Authorize]
        public IActionResult Details(string id = null)
        {
            var task = _taskRepository.GetById(id);
            if (string.IsNullOrEmpty(id) || task==null)
                return NotFound();
            IList<UserTask> userTasks = _taskRepository.GetTaskUsers(id);
            List<ApplicationUser> userList = _userManeger.Users.ToList();
            List<ApplicationUser> Users = new List<ApplicationUser>();
            foreach (var userTask in userTasks)
            {
                Users.Add(userList.Find(m => m.Id == userTask.ApplicationUserId));
            }
            TaskUsers taskUsers = new TaskUsers() { task = task, users = Users };
            return View(taskUsers);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Training_Project.Models;
using Training_Project.Repository;
using Training_Project.Services;
using Training_Project.ViewModel;

namespace Training_Project.Controllers
{
    public class UserController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManeger;
        private readonly RoleManager<IdentityRole> _roleManeger;
        private readonly ITaskRepository _taskRepository;
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IMessageRepository _messageRepository;
        private readonly IMessageRecipientRepository _messageRecipientRepository;
        private readonly INotificationSender _notificationSender;

        public UserController(INotificationSender notificationSender, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, ITaskRepository taskRepository,
             IDepartmentRepository departmentRepository, IMessageRepository messageRepository,
             IMessageRecipientRepository messageRecipientRepository)
        {
            _notificationSender = notificationSender;
            _messageRepository = messageRepository;
            _messageRecipientRepository = messageRecipientRepository;
            _userManeger = userManager;
            _roleManeger = roleManager;
            _taskRepository = taskRepository;
            _departmentRepository = departmentRepository;
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> List(string search = null)
        {
            List<ApplicationUser> users = _userManeger.Users.ToList();
            List<UserDetailViewModel> usersDetail = new List<UserDetailViewModel>();
            foreach (var user in users)
            {
                //Can also Assign Tasks
                var roles = await _userManeger.GetRolesAsync(user);
                var userRoles = roles.ToList();
                usersDetail.Add(new UserDetailViewModel()
                {
                    userRoles = userRoles,
                    applicationUser = user
                });
            }
            if (search != null)
                usersDetail = usersDetail.Where(m => m.applicationUser.Email.Contains(search) || m.applicationUser.UserName.Contains(search) ||
                m.userRoles.Any(role => role.Contains(search))
            ).ToList();
            return View(usersDetail);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> AsignRole(string userName="")
        {
            var UserRole = new UserInputViewModel();
            var user = await _userManeger.FindByNameAsync(userName);
            if (user == null)
                return NotFound();
            UserRole.Email = user.Email;
            UserRole.Roles = _roleManeger.Roles.ToList();
            UserRole.UserName = user.UserName;
            return View(UserRole);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AsignRole(UserInputViewModel userRole)
        {
            var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
            var user = await _userManeger.FindByEmailAsync(userRole.Email);
            if (user == null || String.IsNullOrEmpty(userRole.Role))
                return NotFound();
            var result = await _userManeger.AddToRoleAsync(user, userRole.Role);
            if (result.Succeeded)
            {
                //send SignalR msg
                string messageContent = user.UserName + " you have been assigned a New Role.";
                _notificationSender.SendNotificationToUser(messageContent,  user.Id, loggedInUser.Id, null);
                return RedirectToAction("Details", new { email = user.Email });
            }
            foreach (var error in result.Errors)
                ModelState.AddModelError(string.Empty, error.Description);
            userRole.Roles = _roleManeger.Roles.ToList();
            return View(userRole);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> RemoveRole(string Email="")
        {
            var user = await _userManeger.FindByEmailAsync(Email);
            if (user == null)
                return NotFound();
            var UserRole = new RoleRemoveViewModel()
            {
                Email = Email,
                Roles = await _userManeger.GetRolesAsync(user),
                UserName = user.UserName
            };
            return View(UserRole);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RemoveRole(RoleRemoveViewModel UserRole)
        {
            var user = await _userManeger.FindByEmailAsync(UserRole.Email);
            if (user == null || String.IsNullOrEmpty(UserRole.Role))
                return NotFound();
            var result = await _userManeger.RemoveFromRoleAsync(user, UserRole.Role);
            if (result.Succeeded)
                return RedirectToAction("Index", "Home");
            foreach (var error in result.Errors)
                ModelState.AddModelError(string.Empty, error.Description);
            return RedirectToAction("Index", "Home", new { Email = UserRole.Email });
        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AssignTask(string email="")
        {
            UserTasks userTask = new UserTasks();
            userTask.user = await _userManeger.FindByEmailAsync(email);
            if (userTask.user == null)
                return NotFound();
            var allTasks = _taskRepository.GetAll().ToList();
            foreach(var task in _taskRepository.GetUserTask(userTask.user.Id).ToList())
            {
                allTasks.Remove(task);
            }
            userTask.tasks = allTasks;
            return View(userTask);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AssignTask(string userId, string taskId)
        {
            var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
            userId = userId.Substring(0, userId.Length - 1);
            var user = await _userManeger.FindByIdAsync(userId);
            var task = _taskRepository.GetById(taskId);
            if (user == null || task == null)
                return NotFound();
            if (_taskRepository.AssignTask(userId, taskId))
            {
                //send SignalR msg
                string messageContent = user.UserName + " you have been assigned a New Task.";
                _notificationSender.SendNotificationToUser(messageContent, userId, loggedInUser.Id,null);

                return RedirectToAction("List", "Task", new { UserId = userId });
            }
            ModelState.AddModelError(string.Empty, "This task is already assigned");
            var allTasks = _taskRepository.GetAll().ToList();
            foreach (var usertask in _taskRepository.GetUserTask(user.Id).ToList())
            {
                allTasks.Remove(usertask);
            }
            UserTasks userTask = new UserTasks()
            {
                user = user,
                tasks = allTasks
            };
            return View(userTask);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult AssignDepartment(string email, string department)
        {
            if (String.IsNullOrEmpty(email))
                return NotFound();
            UserDepartment userDepartment = new UserDepartment();
            userDepartment.UserEmail = email;
            userDepartment.Departments = _departmentRepository.GetAll().ToList();
            userDepartment.Department = department;
            return View(userDepartment);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AssignDepartment(UserDepartment userDepartment)
        {
            var loggedInUser =await _userManeger.FindByNameAsync(User.Identity.Name);
            var user = await _userManeger.FindByEmailAsync(userDepartment.UserEmail);
            if (user == null || userDepartment.Department == null)
                return NotFound();
            user.DepartmentId = userDepartment.Department;
            await _userManeger.UpdateAsync(user);
            string messageContent = user.UserName + " Your Department has been Changed.";
            _notificationSender.SendNotificationToUser(messageContent, user.Id, loggedInUser.Id,null);
            return RedirectToAction("Details", "User", new { email = userDepartment.UserEmail });
        }


        [Authorize]
        public async Task<IActionResult> Details(string email)
        {
            if (String.IsNullOrEmpty(email))
                return NotFound();
            var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
            var searchedUser = await _userManeger.FindByEmailAsync(email);
            if (searchedUser == null)
                return NotFound();
            if (loggedInUser.Id == searchedUser.Id)
                return RedirectToAction("MyProfile", "User");
            if (!User.IsInRole("Admin"))
            {
                if (loggedInUser.DepartmentId != searchedUser.DepartmentId)
                {
                    return Redirect("/Identity/Account/AccessDenied");
                }
            }
            UserDetailViewModel user = new UserDetailViewModel();
            user.applicationUser = searchedUser;
            if (user.applicationUser.DepartmentId != null)
                user.applicationUser.Department = _departmentRepository.GetById(user.applicationUser.DepartmentId);
            else
                user.applicationUser.Department = null;
            var roles = await _userManeger.GetRolesAsync(user.applicationUser);
            user.userRoles = roles.ToList();
            user.userTasks = _taskRepository.GetUserTask(user.applicationUser.Id).ToList();
            return View(user);
        }
        [Authorize]
        public async Task<IActionResult> MyProfile()
        {
            var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
            UserDetailViewModel user = new UserDetailViewModel();
            user.applicationUser = loggedInUser;
            if (user.applicationUser.DepartmentId != null)
                user.applicationUser.Department = _departmentRepository.GetById(user.applicationUser.DepartmentId);
            else
                user.applicationUser.Department = null;
            var roles = await _userManeger.GetRolesAsync(user.applicationUser);
            user.userRoles = roles.ToList();
            user.userTasks = _taskRepository.GetUserTask(user.applicationUser.Id).ToList();
            return View("Details", user);
        }
        [Authorize]
        public async Task<IActionResult> MyDepartment()
        {
            var user = await _userManeger.FindByNameAsync(User.Identity.Name);
            return RedirectToAction("Details", "Department", new { id = user.DepartmentId });
        }

        [Authorize]
        public async Task<IActionResult> Tasks(string search = null)
        {
            var user = await _userManeger.FindByNameAsync(User.Identity.Name);
            var userTasks = _taskRepository.GetUserTask(user.Id);
            List<ViewModel.TaskStatus> tasksDetail = new List<ViewModel.TaskStatus>();
            foreach (var task in userTasks)
            {
                tasksDetail.Add(new ViewModel.TaskStatus()
                {
                    task = task,
                    taskStatus = _taskRepository.GetTaskStatus(task.TaskId, user.Id) == true ? "COMPLETED" : "In Progress"
                });
            }
            if (search != null)
            {
                tasksDetail = tasksDetail.Where(m => m.task.Type.Contains(search) ||
                m.task.Description.Contains(search) ||
                m.taskStatus.Contains(search)).ToList();
            }
            return View(tasksDetail);
        }
        [Authorize]
        public async Task<IActionResult> ChangeTaskStatus(string TaskId)
        {
            var user = await _userManeger.FindByNameAsync(User.Identity.Name);
            _taskRepository.ChangeUserTaskStatus(user.Id, TaskId);
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public async Task<IActionResult> Notifications()
        {
            var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
            List<MessageRecipient> myMessages = _messageRecipientRepository.Find(loggedInUser.Id);
            List<Message> notifications = new List<Message>();
            foreach (var message in myMessages)
            {
                var notification = _messageRepository.FindByMessageId(message.MessageId);
                bool isNotification = notification.Type == "Notification" ? true : false;
                if (isNotification)
                    notifications.Add(notification);
            }
            notifications.Sort((y, x) => x.SendingTime.CompareTo(y.SendingTime));
            return View(notifications);
        }
        [Authorize] 
        public async Task<string> GetNotifications(string userName)
        {
            var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
            List<MessageRecipient> myMessages = _messageRecipientRepository.Find(loggedInUser.Id);
            List<Message> notifications = new List<Message>();
            foreach (var message in myMessages)
            {
                var notification = _messageRepository.FindByMessageId(message.MessageId);
                bool isNotification = notification.Type == "Notification" ? true : false;
                if (isNotification)
                    notifications.Add(notification);
                if (notifications.Count == 10)
                    break;
            }
            notifications.Sort((y, x) => x.SendingTime.CompareTo(y.SendingTime));
            if (notifications.Count != 0)
            { 
                string notificationJSON = _notificationSender.getMessageResponse(notifications);
                return notificationJSON;
            }
            return "[]";
        }

        [Authorize]
        public async Task<string> GetMessages(string userName)
        {
            var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
            List<MessageRecipient> myMessages = _messageRecipientRepository.Find(loggedInUser.Id);
            List<Message> messages = new List<Message>();
            foreach (var message in myMessages)
            {
                var notification = _messageRepository.FindByMessageId(message.MessageId);
                bool isMessage = notification.Type == "Message" ? true : false;
                if (isMessage)
                    messages.Add(notification);
                if (messages.Count == 1000)
                    break;
            }
            messages.Sort((y, x) => x.SendingTime.CompareTo(y.SendingTime));
            var groupedMessages = from element in messages
                      group element by element.SenderId
                  into groups
                      select groups.First();
            if (groupedMessages.ToList().Count != 0)
            {
                string messagesJSON = _notificationSender.getMessageResponse(groupedMessages.ToList());
                return messagesJSON;
            }
            return "[]";
        }

        [Authorize]
        public async Task<IActionResult> Inbox(string userName,string recipientId)
        {
            var loggedInUser = await _userManeger.FindByNameAsync(User.Identity.Name);
            List<Message> inbox = new List<Message>();
            List<Message> myMessage = _messageRepository.FindBySenderId(loggedInUser.Id).Where(m=>m.Type=="Message").ToList();
            //Sort messages based on Sending Time
            myMessage.Sort((y, x) => x.SendingTime.CompareTo(y.SendingTime));
            var myTopMessages = myMessage.Take(500).ToList();

            List<Message> userMessage = _messageRepository.FindBySenderId(recipientId).Where(m => m.Type == "Message").ToList();
            //Sort messages based on Sending Time
            userMessage.Sort((y, x) => x.SendingTime.CompareTo(y.SendingTime));
            var userTopMessages = userMessage.Take(500).ToList();
            bool isSent;
            //Check MyMessages Recipient
            foreach (var message in myTopMessages)
            {
                isSent= _messageRecipientRepository.isMessageSent(message.Id, recipientId);
                if (isSent)
                    inbox.Add(message);
            }
            //Check UserMessages Recipent
            foreach (var message in userTopMessages)
            {
                isSent = _messageRecipientRepository.isMessageSent(message.Id, loggedInUser.Id);
                if (isSent)
                    inbox.Add(message);
            }
            inbox.Sort((y, x) => x.SendingTime.CompareTo(y.SendingTime));
            var messagesResponse = _notificationSender.getMessageResponseForInbox(inbox);
            return View(messagesResponse);
        }
    }
}

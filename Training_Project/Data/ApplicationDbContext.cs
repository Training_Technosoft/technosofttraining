﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Training_Project.Models;
using Training_Project.Controllers;
using Microsoft.AspNetCore.Identity;
using Training_Project.Services;
using Training_Project.ViewModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Training_Project.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<FileUser> Files { set; get; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<MessageRecipient> MessageRecipients { get; set; }
        public DbSet<UserTask> UserTasks { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserTask>().HasKey(m => new { m.TaskId,m.ApplicationUserId });
            builder.Entity<MessageRecipient>().HasKey(m => new { m.MessageId,m.RecipientId });

            const string ADMIN_ID = "b4280b6a-0613-4cbd-a9e6-f1701e926e73";
            const string ROLE_ID = ADMIN_ID;
            const string DEPARTMENT_ID = ROLE_ID;
            builder.Entity<IdentityRole>().HasData(new List<IdentityRole>
            {
                new IdentityRole {
                    Id = ROLE_ID,
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                }
            });
            Department department = new Department()
            {
                DepartmentId = DEPARTMENT_ID,
                DepartmentName = "Administration"
            };
            builder.Entity<Department>().HasData(department);
            var hasher = new PasswordHasher<ApplicationUser>();
            builder.Entity<ApplicationUser>().HasData(
                new ApplicationUser
                {
                    Id = ADMIN_ID,
                    UserName = "admin",
                    NormalizedUserName = "admin".ToUpper(),
                    Email = "admin@techno-soft.com",
                    NormalizedEmail = "admin@techno-soft.com".ToUpper(),
                    PasswordHash = hasher.HashPassword(null, "Apollo.12"),
                    EmailConfirmed = true,
                    DepartmentId = DEPARTMENT_ID
                }) ;
            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>
            {
                RoleId = ROLE_ID,
                UserId = ADMIN_ID
            });
            builder.Entity<Department>()
                .HasMany(p => p.Users)
                .WithOne(t => t.Department)
                .OnDelete(DeleteBehavior.SetNull);
        }

        public DbSet<Training_Project.Services.MessageResponse> MessageResponse { get; set; }
    }

}


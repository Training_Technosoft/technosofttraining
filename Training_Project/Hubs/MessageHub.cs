﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Training_Project.Models;
using Training_Project.Repository;
using Training_Project.Services;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using System.IO;

namespace Training_Project.Hubs
{
    public class MessageHub : Hub
    {
        public static ConnectionMapping<string> _connections =
           new ConnectionMapping<string>();
        private readonly UserManager<ApplicationUser> _userManeger;
        private readonly RoleManager<IdentityRole> _roleManeger;
        private readonly IMessageRepository _messageRepository;
        private readonly IMessageRecipientRepository _messageRecipientRepository;
        public MessageHub( IMessageRepository messageRepository,IMessageRecipientRepository messageRecipientRepository,
            UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _messageRepository = messageRepository;
            _messageRecipientRepository = messageRecipientRepository;
            _userManeger = userManager;
            _roleManeger = roleManager;
        }
        public override  System.Threading.Tasks.Task OnConnectedAsync()
        {
            string userIdentifier = Context.UserIdentifier;
            _connections.Add(userIdentifier, Context.ConnectionId);

            //Get User Unread Messages
            var messages = _messageRecipientRepository.Find(userIdentifier, false);
            if(messages.Count==0)
                return base.OnConnectedAsync();
            _messageRecipientRepository.ChangeMessageStatus(userIdentifier);
            List<Message> myMessages = new List<Message>();
            List<Message> myNotifications = new List<Message>();
            foreach (var message in messages)
            {
                var unreadMessage = _messageRepository.FindByMessageId(message.MessageId);
                if(unreadMessage.Type=="Message")
                      myMessages.Add(_messageRepository.FindByMessageId(message.MessageId));
                else
                    myNotifications.Add(_messageRepository.FindByMessageId(message.MessageId));
            }
            if(myMessages.Count!=0)
            {
                string messageJSON = getMessageResponse(myMessages);
                Clients.Client(Context.ConnectionId).SendAsync("ReceiveMessage", messageJSON);
            }
            if(myNotifications.Count!=0)
            {
                string notificationJSON = getMessageResponse(myNotifications);
                Clients.Client(Context.ConnectionId).SendAsync("ReceiveNotification", notificationJSON);
            }
            return base.OnConnectedAsync();
        }
        public override System.Threading.Tasks.Task OnDisconnectedAsync(Exception exception)
        {
            string userIdentifier = Context.UserIdentifier;

            _connections.Remove(userIdentifier, Context.ConnectionId);

            return base.OnDisconnectedAsync(exception);
        }
        public  void OnReconnectedAsync()
        {
            string userIdentifier = Context.UserIdentifier;

            if (!_connections.GetConnections(userIdentifier).Contains(Context.ConnectionId))
            {
                _connections.Add(userIdentifier, Context.ConnectionId);
            }
        }


        public async System.Threading.Tasks.Task SendMessageToGroup(string messageBody, string groupName)
        {
            Message message = new Message()
            {
                MessageBody = messageBody,
                SenderId = Context.UserIdentifier,
                SendingTime = DateTime.Now,
                Type = "Message"
            };
            _messageRepository.Create(message);
            var users =await _userManeger.GetUsersInRoleAsync(groupName);
            users = users.ToList();
            foreach(var user in users)
            {
                SendMessageToUser(messageBody, user.Id,message);
            }
        }

        public  void SendMessageToUser(string messageBody,string recipientId,Message message)
        {
            var clientConnection = _connections.GetConnections(recipientId).ToList();

            bool isRecipientActive = clientConnection.Count!=0 ? true : false;
            if (message == null)
            {
                message = new Message()
                {
                    MessageBody = messageBody,
                    SenderId = Context.UserIdentifier,
                    SendingTime = DateTime.Now,
                    Type = "Message"
                };
                _messageRepository.Create(message);
            }
            List<Message> messages = new List<Message>();
            messages.Add(message);
            string messageJSON= getMessageResponse(messages);
            foreach (var connectionId in clientConnection)
            {
                Clients.Client(connectionId).SendAsync("ReceiveMessage",messageJSON);
            }

            _messageRecipientRepository.Create(recipientId, message.Id, isRecipientActive);
        }
        public  string getMessageResponse(List<Message> messages)
        {
            List<MessageResponse> messageResponses = new List<MessageResponse>();
            foreach (var message in messages)
            {
                var user = _userManeger.Users.Where(m => m.Id == message.SenderId).FirstOrDefault();
                messageResponses.Add( new MessageResponse()
                {
                    MessageBody = message.MessageBody,
                    SenderName = user.UserName,
                    SenderId = message.SenderId,
                    UserPictureURL = "/UserImages/"+ Path.GetFileName( user.ProfilePicturePath),
                    Date = message.SendingTime.ToShortDateString(),
                    Time = message.SendingTime.ToShortTimeString()
                });
            }
            messageResponses.OrderBy(m => m.Date).ThenBy(m => m.Time);
            return JsonConvert.SerializeObject(messageResponses);
        }
    }
}

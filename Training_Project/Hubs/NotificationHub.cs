﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Training_Project.Models;
using Training_Project.Repository;

namespace Training_Project.Hubs
{
    public class NotificationHub : Hub
    {


        public async System.Threading.Tasks.Task Announcement(string message)
        {
            await Clients.AllExcept(Context.ConnectionId).SendAsync("ReceiveMessage", message,"Admin");
        }


    }
}
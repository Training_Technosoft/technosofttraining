﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Training_Project.Migrations
{
    public partial class Auto_IncrementedId_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "bcfc590b-33d9-4ad0-b72f-e9cf547331f3");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b0e118fd-6d8a-4969-a71b-b5012b03ffce", "AQAAAAEAACcQAAAAEHyb61TCVc350b4+Ah7wCZvyqlWe/Tu0+vUqTJo75IZ2i7h9CLIPI/z9LZ4aSK1wiw==", "685ec70a-1875-4f87-a5c6-77f856481e2c" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "6b3889d0-5a73-4410-8509-c8e026d6c960");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "45020cbd-a52f-4c9b-8ac0-4b73e5168a89", "AQAAAAEAACcQAAAAECOXM9i7kOHL+HzS2K9VIPdK3p9jNzPKhsYYB2IpIIqtOpKeS1MZ5WS71f0DsoINSA==", "26ebff5c-4924-439c-8849-50bb609ee845" });
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Training_Project.Migrations
{
    public partial class IsBlocked_Added_User : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "Tasks",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "isBlock",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "19b2a832-6ea7-44a3-bd07-0d3ba64da4da");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "2a9bab4e-fd79-4122-a9c4-6992b9ff8b62", "AQAAAAEAACcQAAAAEAdORUBHGaLfdbtOazOcBl8WQB9RiDZf7Txh+1QvOjXqeXKTmhLfq6qEAzLrA92V0g==", "8cae0458-fccc-411f-a7f1-48568ae2c271" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isBlock",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "Tasks",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "6178d77f-036b-4602-8510-eb58973c7525");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e10d4d27-f065-4ab5-b0e5-0798580ed46e", "AQAAAAEAACcQAAAAEOL25n13YYk37SrdqXH8uTcDKU/BQNqPTyTfk8cr4y+0Uu+lWn7R6s0yWrB3E8RW4Q==", "4c54b751-2a6a-4907-9ed3-aed132c39016" });
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Training_Project.Migrations
{
    public partial class AddedStatusIn_UserTask_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isCompleted",
                table: "UserTasks",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "6d4be0f3-c94d-4ae3-b5ce-3c90874dbcf3");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cc9014b6-5a63-4463-8de8-8b035cf42049", "AQAAAAEAACcQAAAAEMNa7ar7qgbPEzvcUIIbybtODHVV8YmMc5ePbkRcoZ2WqJS9JwxlLF4PVvUfLvT+5w==", "8823df9e-6c04-4f10-868c-20228756736a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isCompleted",
                table: "UserTasks");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "b84737af-ec5e-415e-a6af-ea22d257790e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3ffcd9e5-f43d-4d67-ad93-0c451655cbaf", "AQAAAAEAACcQAAAAEA7d3dPkwt3uyqneTmyAZm1WoOKXReTZeHtJwQciI/uf261iPJfe7Q+zPXtylrvpuA==", "14316223-48ac-4bc9-aa2a-61c18b72f7fc" });
        }
    }
}

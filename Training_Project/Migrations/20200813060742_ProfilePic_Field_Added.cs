﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Training_Project.Migrations
{
    public partial class ProfilePic_Field_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProfilePicturePath",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "df2e1fd6-84cf-430a-9a57-1728146287c2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "209f163d-a0dd-417f-8f79-e006178d2b4d", "AQAAAAEAACcQAAAAENyAV15CIoc2XQcunFgvMilew4j6c1EtmlRcn3koP0yVRXckB2n7giBRZzuBYPrPxg==", "895d04dc-baac-48b6-96a8-3e6d510f187b" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProfilePicturePath",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "6d4be0f3-c94d-4ae3-b5ce-3c90874dbcf3");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cc9014b6-5a63-4463-8de8-8b035cf42049", "AQAAAAEAACcQAAAAEMNa7ar7qgbPEzvcUIIbybtODHVV8YmMc5ePbkRcoZ2WqJS9JwxlLF4PVvUfLvT+5w==", "8823df9e-6c04-4f10-868c-20228756736a" });
        }
    }
}

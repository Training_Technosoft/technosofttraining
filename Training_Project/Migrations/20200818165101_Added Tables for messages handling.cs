﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Training_Project.Migrations
{
    public partial class AddedTablesformessageshandling : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClientConnection",
                columns: table => new
                {
                    ConnectionId = table.Column<string>(nullable: false),
                    UserId = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientConnection", x => x.ConnectionId);
                    table.ForeignKey(
                        name: "FK_ClientConnection_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Message",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    MessageBody = table.Column<string>(nullable: false),
                    SenderId = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    ApplicationUserId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Message_AspNetUsers_ApplicationUserId",
                        column: x => x.ApplicationUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MessageRecipient",
                columns: table => new
                {
                    RecipientId = table.Column<string>(nullable: false),
                    MessageId = table.Column<string>(nullable: false),
                    IsDeleivered = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageRecipient", x => new { x.MessageId, x.RecipientId });
                    table.ForeignKey(
                        name: "FK_MessageRecipient_Message_MessageId",
                        column: x => x.MessageId,
                        principalTable: "Message",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "ef990157-bb8d-47ec-86e9-df1e1bad1644");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "1c3ef4c0-c59e-4f19-9701-460aa667c822", "AQAAAAEAACcQAAAAEDqoBkADCUNm/xWpias807OGqawGLDybtAgkBTo2BgHiJczbkfiOmkHB6wddzInaTQ==", "a9da892f-2176-4fde-8611-537f95098682" });

            migrationBuilder.CreateIndex(
                name: "IX_ClientConnection_ApplicationUserId",
                table: "ClientConnection",
                column: "ApplicationUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Message_ApplicationUserId",
                table: "Message",
                column: "ApplicationUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientConnection");

            migrationBuilder.DropTable(
                name: "MessageRecipient");

            migrationBuilder.DropTable(
                name: "Message");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "df2e1fd6-84cf-430a-9a57-1728146287c2");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "209f163d-a0dd-417f-8f79-e006178d2b4d", "AQAAAAEAACcQAAAAENyAV15CIoc2XQcunFgvMilew4j6c1EtmlRcn3koP0yVRXckB2n7giBRZzuBYPrPxg==", "895d04dc-baac-48b6-96a8-3e6d510f187b" });
        }
    }
}

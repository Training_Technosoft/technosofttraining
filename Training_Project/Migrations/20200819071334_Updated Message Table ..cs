﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Training_Project.Migrations
{
    public partial class UpdatedMessageTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "SendingTime",
                table: "Messages",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "55dca788-8264-4db4-84ed-67597bd72c98");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4a7a0343-42c2-4e3f-aa79-a419d76f475f", "AQAAAAEAACcQAAAAEM4uSvZQT6pM0HpEAEa1izjOg6ULa+1r6ZVBFrhWcgDPDjrc7kwz+/b1q96RX5m2fw==", "277705c7-d8f1-415a-86e6-bbc98479884d" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SendingTime",
                table: "Messages");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "6417792a-c686-4fa5-80a2-d644cc7ec72c");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "52fe1add-a2de-4d55-aa6c-268fd11d6e6d", "AQAAAAEAACcQAAAAEAQC40v25YE3ykbK5RSnRA5z1OQc4GZ6zDVgs9lfa3j7rwhpHcwAl4aTh/ICFy2mKg==", "4d14dd67-f663-41b1-b54b-5ab865cdf09a" });
        }
    }
}

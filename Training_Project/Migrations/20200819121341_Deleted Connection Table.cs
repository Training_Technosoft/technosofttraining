﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Training_Project.Migrations
{
    public partial class DeletedConnectionTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientConnections_AspNetUsers_UserId",
                table: "ClientConnections");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientConnections",
                table: "ClientConnections");

            migrationBuilder.RenameTable(
                name: "ClientConnections",
                newName: "ClientConnection");

            migrationBuilder.RenameIndex(
                name: "IX_ClientConnections_UserId",
                table: "ClientConnection",
                newName: "IX_ClientConnection_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientConnection",
                table: "ClientConnection",
                column: "ConnectionId");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "7c873a92-1134-41a2-b6ff-ae9a71a02105");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "db170e27-f60c-4fbd-9145-43d58d406fc7", "AQAAAAEAACcQAAAAED5vpd4syjis6bwRXmEBYaJi3Tjdu0ryl68+gXFW5I9VW9JKDKMfQQUwMdcqhLW0vQ==", "200c31e2-f271-404e-98b3-d5b7066c1bf0" });

            migrationBuilder.AddForeignKey(
                name: "FK_ClientConnection_AspNetUsers_UserId",
                table: "ClientConnection",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClientConnection_AspNetUsers_UserId",
                table: "ClientConnection");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ClientConnection",
                table: "ClientConnection");

            migrationBuilder.RenameTable(
                name: "ClientConnection",
                newName: "ClientConnections");

            migrationBuilder.RenameIndex(
                name: "IX_ClientConnection_UserId",
                table: "ClientConnections",
                newName: "IX_ClientConnections_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClientConnections",
                table: "ClientConnections",
                column: "ConnectionId");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                column: "ConcurrencyStamp",
                value: "55dca788-8264-4db4-84ed-67597bd72c98");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "b4280b6a-0613-4cbd-a9e6-f1701e926e73",
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4a7a0343-42c2-4e3f-aa79-a419d76f475f", "AQAAAAEAACcQAAAAEM4uSvZQT6pM0HpEAEa1izjOg6ULa+1r6ZVBFrhWcgDPDjrc7kwz+/b1q96RX5m2fw==", "277705c7-d8f1-415a-86e6-bbc98479884d" });

            migrationBuilder.AddForeignKey(
                name: "FK_ClientConnections_AspNetUsers_UserId",
                table: "ClientConnections",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

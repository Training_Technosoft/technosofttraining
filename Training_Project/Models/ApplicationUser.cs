﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Training_Project.Models
{
    public class ApplicationUser: IdentityUser
    {
        [ForeignKey("Department")]
        public string DepartmentId { get; set; }
        public Department Department { get; set; }
        public bool isBlock { get; set; }
        public ICollection<UserTask> Tasks { get; set; }
        public ICollection<FileUser> Files { get; set; }

        [ForeignKey("MessageId")]
        public ICollection<Message> Messages { get; set; }
        public string ProfilePicturePath { get; set; }

    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Training_Project.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Training_Project.Models
{
    public class FileUser
    {
        [Key]
        public int FileId { get; set; }
        [Required]
        public string FileName { get; set; }
        [Required]
        public string FilePath { get; set; }
        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public FileUser()
        { }
        public FileUser(string name, string url, string id)
        {
            FileName = name;
            FilePath = url;
            UserId = id;
        }

    }
}

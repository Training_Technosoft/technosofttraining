﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Training_Project.Models
{
    public class Message
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }

        [Required]
        public string MessageBody { get; set; }

        [DataType(DataType.Date)]
        public DateTime SendingTime { get; set; }

        [Required]
        public string SenderId { get; set; }


        [Required]
        public string Type { get; set; }

        [ForeignKey("MessageRecipientsId")]
        public ICollection<MessageRecipient> MessageRecipients { get; set; }
    }
}

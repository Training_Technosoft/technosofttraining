﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Training_Project.Models
{
    public class MessageRecipient
    {
        [Required]
        public string RecipientId { get; set; }

        [Required]
        public string MessageId { get; set; }

        [Required]
        public bool IsDeleivered { get; set; }
    }
}

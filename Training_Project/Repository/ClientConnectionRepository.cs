﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Data;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public class ClientConnectionRepository: IClientConnectionRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public ClientConnectionRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool Create(string connectionId, string userId,string userName)
        {
            try
            {
                _dbContext.ClientConnections.Add(new ClientConnection() {
                    ConnectionId=connectionId,
                    UserId=userId,
                    UserName=userName
                });
                Save();
                return true;
            }
            catch(Exception expection)
            {
                return false;
            }
        }
        public bool Remove(string connectionId)
        {
            try
            {
                var connection=_dbContext.ClientConnections.Where(m => m.ConnectionId == connectionId).FirstOrDefault();
                _dbContext.Remove(connection);
                Save();
                return true;
            }
            catch (Exception expection)
            {
                return false;
            }
        }
        public List<ClientConnection> Find(string userId)
        {
            var connectiontions = _dbContext.ClientConnections.Where(m => m.UserId == userId).ToList();
            return connectiontions;
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}
*/
﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Data;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public class DepartmentRepository :IDepartmentRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public DepartmentRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void Create(Department department)
        {
            bool isPresent = _dbContext.Departments.Any(m => m.DepartmentName == department.DepartmentName);
            if (isPresent)
                return;
            _dbContext.Departments.Add(department);
            Save();
        }
        public Department GetById(string Id)
        {
            return _dbContext.Departments.Find(Id);
        }
        public IList<Department> GetAll()
        {
            return _dbContext.Departments.ToList();
        }
        public bool isDepartmentPresent(string Name)
        {
            bool isPresent = _dbContext.Departments.Any(m => m.DepartmentName == Name);
            return isPresent;
        }
        public bool Update(Department department)
        {
            bool isPresent = _dbContext.Departments.Any(m => m.DepartmentName == department.DepartmentName && m.DepartmentId != department.DepartmentId);
            if (!isPresent)
            {
                _dbContext.Departments.Update(department);
                Save();
                return true;
            }
            return false;

        }
        public void Delete(string Id)
        {
            Department department = _dbContext.Departments.Find(Id);
            if (department == null)
                return;
            _dbContext.Departments.Remove(department);
            Save();
        }
        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}

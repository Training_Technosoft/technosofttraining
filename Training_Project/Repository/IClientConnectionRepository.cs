﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public interface  IClientConnectionRepository
    {
        bool Create(string connectionId, string userId, string userName);
        bool Remove(string connectionId);
        List<ClientConnection> Find(string userId);
        void Save();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public interface IDepartmentRepository 
    {
        void Create(Department department);
        Department GetById(string Id);
        bool isDepartmentPresent(string Name);
        IList<Department> GetAll();
        bool Update(Department department);
        void Delete(string Id);
        void Save();
    }
}

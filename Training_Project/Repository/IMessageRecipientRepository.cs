﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public interface IMessageRecipientRepository
    {
        bool Create(string recipietnId,string messageId,bool isDelievered);
        List<MessageRecipient> Find(string recipientId);
        bool isMessageSent(string messageId,string recipientId);
        List<MessageRecipient> Find(string recipientId,bool isDelievered);
        void ChangeMessageStatus(string recipientId);
        void Save();
    }
}

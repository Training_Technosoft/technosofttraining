﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public interface IMessageRepository
    {
        bool Create(Message message);
        Message FindByMessageId(string messageId);
        List<Message> FindBySenderId(string senderId);
        void Save();
    }
}

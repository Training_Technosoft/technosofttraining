﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Training_Project.Data;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public interface ITaskRepository
    {
        bool Create(Task task);
        Task GetById(string Id);
        IList<Task> GetAll();
        bool ChangeUserTaskStatus(string userId, string taskId,bool status=true);
        bool Update(Task task);
        bool Delete(string Id);
        bool AssignTask(string UserId, string TaskId);
        IList<Task> GetUserTask(string userId);
        bool GetTaskStatus(string taskId, string userId);
        public IList<UserTask> GetTaskUsers(string taskId);
        void Save();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Data;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public class MessageRecipientRepository :IMessageRecipientRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public MessageRecipientRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public bool Create(string recipietnId, string messageId, bool isDelievered)
        {
            try
            {
                _dbContext.MessageRecipients.Add(new MessageRecipient()
                {
                    RecipientId = recipietnId,
                    MessageId = messageId,
                    IsDeleivered=isDelievered
                }) ;
                Save();
                return true;
            }
            catch(Exception exception)
            {
                return false;
            }
        }
        public List<MessageRecipient> Find(string recipientId)
        {
            var messageRecipientList = _dbContext.MessageRecipients.Where(m => m.RecipientId == recipientId).ToList();
            return messageRecipientList;
        }
        public List<MessageRecipient> Find(string recipientId, bool isDelievered)
        {
            var messageRecipientList = _dbContext.MessageRecipients.Where(m => m.RecipientId == recipientId && m.IsDeleivered==isDelievered).ToList();
            return messageRecipientList;
        }

        public void ChangeMessageStatus(string recipientId)
        {
            var messageRecipientList = Find(recipientId,false);
            foreach(var message in messageRecipientList)
            {
                message.IsDeleivered = true;
                _dbContext.MessageRecipients.Update(message);
            }
            Save();
        }
        public bool isMessageSent(string messageId, string recipientId)
        {
            var message=_dbContext.MessageRecipients.Where(m => m.RecipientId == recipientId && m.MessageId == messageId).FirstOrDefault();
            if (message == null)
                return false;
            return true;
        }
        public void Save()
        {
            _dbContext.SaveChanges();

        }
    }
}

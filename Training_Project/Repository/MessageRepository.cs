﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Data;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public class MessageRepository: IMessageRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public MessageRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public bool Create(Message message)
        {
            try
            {
                _dbContext.Messages.Add(message);
                Save();
                return true;
            }
            catch(Exception exception)
            {
                return false;
            }
        }
        public List<Message> FindBySenderId(string senderId)
        {
            List<Message> messages = _dbContext.Messages.Where(m => m.SenderId == senderId).ToList();
            return messages;
        }

        public Message FindByMessageId(string messageId)
        {
            Message message = _dbContext.Messages.Where(m => m.Id == messageId).ToList().FirstOrDefault();
            return message;
        }
        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Training_Project.Data;
using Training_Project.Models;

namespace Training_Project.Repository
{
    public class TaskRepository : ITaskRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public TaskRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool Create(Task task)
        {
           bool isPresent= _dbContext.Tasks.Any(m => m.Description == task.Description && m.Type==task.Type);
            if (isPresent)
                return false;
            _dbContext.Add(task);
            Save();
            return true;
        }
        public Task GetById(string Id)
        {
            return _dbContext.Tasks.Find(Id);
        }
        public IList<Task> GetAll()
        {
            return _dbContext.Tasks.ToList();
        }
        public bool Update(Task task)
        {
            Task taskCheck = _dbContext.Tasks.Where(m => m.Description == task.Description && m.Type == task.Type && task.TaskId != m.TaskId).FirstOrDefault();
            if (taskCheck!=null)
                return false;
            _dbContext.Tasks.Update(task);
            Save();
            return true;
        }
        public bool Delete(string Id)
        {
            Task task = _dbContext.Tasks.Find(Id);
            if (task == null)
                return false;
            _dbContext.Tasks.Remove(task);
            Save();
            return true;
        }
        public bool AssignTask(string userId, string taskId)
        {
            bool isPresent = _dbContext.UserTasks.Any(m => m.ApplicationUserId == userId && m.TaskId == taskId);
            if (isPresent )
                return false;
            UserTask userTask=new UserTask() { ApplicationUserId = userId, TaskId = taskId };
            _dbContext.UserTasks.Add(userTask);
            Save();
            return true;
        }
        public IList<Task> GetUserTask(string userId)
        {
            List<UserTask> userTasks = _dbContext.UserTasks.Where(m => m.ApplicationUserId == userId).ToList();
            IList<Task> tasks = new List<Task>(); 
            foreach(var task in userTasks)
            {
                tasks.Add(GetById(task.TaskId));
            }
            return tasks;
        }

        public bool ChangeUserTaskStatus(string userId, string taskId, bool status = true)
        {
            var userTask = _dbContext.UserTasks.Where(m => m.ApplicationUserId == userId && m.TaskId == taskId).ToList().First();
            if (userTask == null)
                return false;
            userTask.isCompleted = status;
            _dbContext.UserTasks.Update(userTask);
            Save();
            return true;
        }

        public bool GetTaskStatus(string taskId, string userId)
        {
            var userTask = _dbContext.UserTasks.Where(m => m.ApplicationUserId == userId && m.TaskId == taskId).ToList().First();
            return userTask.isCompleted;
        }
        public IList<UserTask> GetTaskUsers(string taskId)
        {
            return _dbContext.UserTasks.Where(m => m.TaskId == taskId).ToList();
        }
        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}

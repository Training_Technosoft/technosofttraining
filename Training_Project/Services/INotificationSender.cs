﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.Services
{
    public interface  INotificationSender
    {
        bool SendNotificationToUser(string messageContent, string recipentId,string senderId,Message message);
        string getMessageResponse(List<Message> messages);
        List<MessageResponse> getMessageResponseForInbox(List<Message> messages);
    }
}

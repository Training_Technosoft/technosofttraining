﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.Services
{
    public class MessageResponse
    {
        [Key]
        public string Id { get; set; }
        public string MessageBody { get; set; }
        public string SenderId { get; set; }
        public string SenderName { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string UserPictureURL { get; set; }

    }
}

﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Training_Project.Hubs;
using Training_Project.Models;
using Training_Project.Repository;

namespace Training_Project.Services
{
    public class NotificationSender :INotificationSender
    {
        private readonly UserManager<ApplicationUser> _userManeger;
        private readonly IMessageRepository _messageRepository ;
        private readonly IMessageRecipientRepository _messageRecipientRepository;
        private readonly IHubContext<MessageHub> _hubContext;
        public NotificationSender(IMessageRepository messageRepository, IMessageRecipientRepository messageRecipientRepository, 
            IHubContext<MessageHub> hubContext, UserManager<ApplicationUser> userManager)
        {
            _messageRepository = messageRepository;
            _messageRecipientRepository = messageRecipientRepository;
            _userManeger = userManager;
            _hubContext = hubContext;
        }
        public bool SendNotificationToUser(string messageContent,string recipentId,string senderId,Message message)
        {
            try
            {
                if (message == null)
                {
                    message = new Message()
                    {
                        MessageBody = messageContent,
                        SenderId = senderId,
                        Type = "Notification",
                        SendingTime = DateTime.Now
                    };
                    _messageRepository.Create(message);
                }
                var clientConnection = MessageHub._connections.GetConnections(recipentId).ToList();
                if (clientConnection.Count == 0)
                    _messageRecipientRepository.Create(recipentId, message.Id, false);
                else
                    _messageRecipientRepository.Create(recipentId, message.Id, true);
                List<Message> messages = new List<Message>();
                messages.Add(message);
                string messageJSON = getMessageResponse(messages);
                foreach (var connection in clientConnection)
                {
                     _hubContext.Clients.Client(connection).SendAsync("ReceiveNotification", messageJSON);
                }


                return true;
            }
            catch( Exception exception)
            {
                return false;
            }
        }
        public string getMessageResponse(List<Message> messages)
        {
            List<MessageResponse> messageResponses = new List<MessageResponse>();
            foreach (var message in messages)
            {
                var user = _userManeger.Users.Where(m => m.Id == message.SenderId).FirstOrDefault();
                messageResponses.Add(new MessageResponse()
                {
                    MessageBody = message.MessageBody,
                    SenderName = user.UserName,
                    SenderId = message.SenderId,
                    UserPictureURL = "/UserImages/" + Path.GetFileName(user.ProfilePicturePath),
                    Date = message.SendingTime.ToShortDateString(),
                    Time = message.SendingTime.ToShortTimeString()
                });
            }
            messageResponses.OrderBy(m => m.Date).ThenBy(m => m.Time);
            return JsonConvert.SerializeObject(messageResponses);
        }

        public List<MessageResponse> getMessageResponseForInbox(List<Message> messages)
        {
            List<MessageResponse> messageResponses = new List<MessageResponse>();
            foreach (var message in messages)
            {
                var user = _userManeger.Users.Where(m => m.Id == message.SenderId).FirstOrDefault();
                messageResponses.Add(new MessageResponse()
                {
                    MessageBody = message.MessageBody,
                    SenderName = user.UserName,
                    SenderId = message.SenderId,
                    UserPictureURL = "/UserImages/" + Path.GetFileName(user.ProfilePicturePath),
                    Date = message.SendingTime.ToShortDateString(),
                    Time = message.SendingTime.ToShortTimeString()
                });
            }
            return messageResponses;
        }

    }
}


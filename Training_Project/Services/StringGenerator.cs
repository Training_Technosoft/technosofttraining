﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Training_Project.Services
{
    public class StringGenerator
    {

        public static string RandomFileNameGenerator()
        {
            int length = 15;
            var random = new Random((int)DateTime.Now.Ticks);
            byte[] result = new byte[length];
            //Add lower case letters
            for (int index = 0; index < length; index++)
            {
                result[index] = (byte)random.Next(97, 122);
            }
            return System.Text.Encoding.ASCII.GetString(result);
        }
        public static string RandomPasswordGenerator(int length = 6)
        {
            //Passowrd can't be less than 3 characters
            length = length >= 3 ? length : 3;
            var random = new Random((int)DateTime.Now.Ticks);
            byte[] result = new byte[length];
            //Add lower case letters
            for (int index = 0; index < length; index++)
            {
                result[index] = (byte)random.Next(97, 122);
            }
            int randomIndex;
            //Add upercase letters
            for (int i = 0; i < 3; i++)
            {
                randomIndex = random.Next(0, length - 1);
                result[randomIndex] = (byte)random.Next(65, 90);
            }
            //Add numbers (0-9)
            for (int i = 0; i < 2; i++)
            {
                randomIndex = random.Next(0, length - 1);
                result[randomIndex] = (byte)random.Next(48, 57);
            }
            //Add Special Charcter
            randomIndex = random.Next(0, length - 1);
            result[randomIndex] = (byte)random.Next(33, 47);
            return System.Text.Encoding.ASCII.GetString(result);
        }
    }
}

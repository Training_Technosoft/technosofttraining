﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.ViewModel
{
    public class DepartmentUsers
    {
        public string DepartmentId { get; set; }
        public List<Department> Departments { get; set; }

        public string UserId { get; set; }
        public List<ApplicationUser> Users { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Training_Project.ViewModel
{
    public class IndexViewModel
    {
        public UserViewModel LoggedInUser;
        public List<UserViewModel> userViewModels;
        public List<IdentityRole> identityRoles;

    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.ViewModel
{
    public class RoleUsersViewModel
    {
        public IdentityRole Role { get; set; }
        public List<ApplicationUser> Users { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Training_Project.ViewModel
{
    public class TaskStatus
    {
        public Models.Task task { get; set; }
        public string taskStatus { get; set; }
    }
}

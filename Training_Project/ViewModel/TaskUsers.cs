﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.ViewModel
{
    public class TaskUsers
    {
        public List<ApplicationUser> users { get; set; }
        public Models.Task task { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.ViewModel
{
    public class UserDepartment
    {
        public string UserEmail { get; set; }

        public List<Department> Departments { get; set; }

        [Required]
        public string Department { get; set; }
    }
}

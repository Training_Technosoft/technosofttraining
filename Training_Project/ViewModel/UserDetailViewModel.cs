﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.ViewModel
{
    public class UserDetailViewModel
    {
        public ApplicationUser applicationUser { get; set; }
        public List<Models.Task> userTasks { get; set; }
        public List<string> userRoles { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Training_Project.Models;
using Newtonsoft.Json.Serialization;

namespace Training_Project.ViewModel
{
    public class UserInputViewModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Choose a Role")]
        public string Role { get; set; }
        public List<IdentityRole> Roles { get; set; }
        [Required]
        public string Department { get; set; }
        public IList<Department> Departments { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.ViewModel
{
    public class UserTasks
    {
        public ApplicationUser user { get; set; }
        public List<Models.Task> tasks { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training_Project.Models;

namespace Training_Project.ViewModel
{
    public class UserViewModel
    {
        public IList<string> Roles;
        public IList<TaskStatus> Tasks;
        public string UserName;
        public string Email;
        public bool EmailConfirmed;
        public string Department;
        public UserViewModel(ApplicationUser user,IList<string> roles, string department, IList<TaskStatus> tasks=null)
        {
            Email = user.Email;
            UserName = user.UserName;
            Roles = roles;
            EmailConfirmed = user.EmailConfirmed;
            Department = department;
            if (tasks == null)
                tasks = new List<TaskStatus>();
            else
                Tasks = tasks;
        }
    }
}

﻿"use strict";

var isNotificationsFetched = false
var isMessagesFetched = false
var toggle_visibility = (divId,user) => {
    if (divId == "Inbox-Box") {
        $('#Notification-Box').fadeOut(100)
        $('#InboxCount').text('0')
        $('#InboxCount').hide()
        getUserMessages(user)
    }
    else {
        $('#Inbox-Box').fadeOut(100);
        $('#NotificationCount').text('0')
        $('#NotificationCount').hide()
        getUserNotification(user)
    }
    $('#' + divId).fadeToggle(100);
}

var connection = new signalR.HubConnectionBuilder().withUrl("/messagehub").build();
connection.serverTimeoutInMilliseconds = 100000;

connection.start().then(() => { })
    .catch(() => { });


connection.on("ReceiveMessage", function (messageJSON) {
    var messages = JSON.parse(messageJSON)
    console.log(messages)
    var inboxCount = document.getElementById("InboxCount")
    var previousCount = parseInt(inboxCount.innerText)
    inboxCount.style.display = 'inline-block'
    inboxCount.innerText = messages.length + previousCount 
    messages.forEach(message => {
        if (getTodaysDate() == message.Date)
            message.Date = 'Today'
        var messageContent = '<div "><img id="inbox_Img" src=' + message.UserPictureURL + '></div><div><strong>' + message.SenderName + '</strong> <br/><small>' + message.Date + ' ' + message.Time + ' </small> <p>' +
            message.MessageBody + ' </p><hr/></div>'
        $('#Inbox_Content').prepend(messageContent)
    })
    $('#Inbox_Content').prepend('<strong >New Messages</strong><hr/>')
});

connection.on("ReceiveNotification", function (notificationJSON) {
    var notifications = JSON.parse(notificationJSON)
    console.log(notifications)
    var inboxCount = document.getElementById("NotificationCount")
    var previousCount = parseInt(inboxCount.innerText)
    inboxCount.style.display = 'inline-block'
    inboxCount.innerText = notifications.length + previousCount
    notifications.forEach(notifcation => {
        if (getTodaysDate() == notifcation.Date)
            notifcation.Date = 'Today'
        var notificationContent = '<div "><img id="inbox_Img" src=' + notifcation.UserPictureURL + '></div><div><p ><strong>' + notifcation.SenderName + '</strong> <br/><small> ' + notifcation.Date + ' ' + notifcation.Time + ' </small> <br/></div>' +
            notifcation.MessageBody + ' </p><hr/>'
        $('#Notification_Content').prepend(notificationContent)
    })
    $('#Notification_Content').prepend('<strong >New Notifications</strong><hr/>')

})


connection.onreconnected(connectionId => {
    console.log("Reconnected")
    connection.invoke("OnReconnectedAsync").catch(function (err) {
        return console.error(err.toString());
    });
});

var sendMessageToGroup = (event) => {
    event.preventDefault();
    var messageBody = document.getElementById("messageInput").value;
    var role = document.getElementById("Role").value;
    connection.invoke("SendMessageToGroup", messageBody, role).catch(function (err) {
        return console.error(err.toString());
    });
    document.getElementById("modalClose").click()
    document.getElementById("messageBox").style.display = "none"
    document.getElementById("messageForm").reset()
}

var sendMessageToUser = (event) => {
    event.preventDefault();
    var messageBody = document.getElementById("messageInput").value;
    var recipientId = document.getElementById("User").value;
    connection.invoke("SendMessageToUser", messageBody, recipientId, null).catch(function (err) {
        console.log("NOT Sent")
        return console.error(err.toString());
    });
    document.getElementById("modalClose").click()
    document.getElementById("messageBox").style.display = "none"
    document.getElementById("messageForm").reset()
}

var getTodaysDate = () => {
    var today = new Date()
    var dd = String(today.getDate())
    var mm = String(today.getMonth() + 1) //January is 0!
    var yyyy = today.getFullYear()
    today = mm + '/' + dd + '/' + yyyy
    return today
}


var getUserNotification = (user) => {
    if (isNotificationsFetched == false) {
        $('#notificationSpinner').css("display", "block")
        $.ajax('/User/GetNotifications/?userName=' + user, {
            success: function (data, status, xhr) {
                isNotificationsFetched = true
                var notifications = JSON.parse(data)
                $('#notificationSpinner').css("display", "none")
                notifications.forEach(notifcation => {
                    if (getTodaysDate() == notifcation.Date)
                        notifcation.Date = 'Today'
                    var notificationContent = '<div "><img id="inbox_Img" src=' + notifcation.UserPictureURL + '></div><div><p ><strong>' + notifcation.SenderName + '</strong> <br/><small> ' + notifcation.Date + ' ' + notifcation.Time + ' </small> <br/></div>' +
                        notifcation.MessageBody + ' </p><hr/>'
                    $('#Notification_Content').append(notificationContent)
                })
                $('#Notification_Content').append('<a class="text-center" href="/User/Notifications">Load More</a>')
            }
        });
    }
}

var getUserMessages = (user) => {
    if (isMessagesFetched == false) {
        $('#inboxSpinner').css("display", "block")
        $.ajax('/User/GetMessages/?userName=' + user, {
            success: function (data, status, xhr) {
                isMessagesFetched = true
                var messages = JSON.parse(data)
                $('#inboxSpinner').css("display", "none")
                $('#Inbox_Content').append('<strong >Previous Messages</strong><hr/>')
                messages.forEach(message => {
                    if (getTodaysDate() == message.Date)
                        message.Date = 'Today'
                    var clickEvent = "href=/User/Inbox/?userName=" + user + "&recipientId=" + message.SenderId
                    var messageContent = '<div  ><img  id="inbox_Img" src=' + message.UserPictureURL + '></div><div><p ><a '+ clickEvent +' class="inboxClick" ><strong>' + message.SenderName + '</strong></a> <br/><small> ' + message.Date + ' ' + message.Time + ' </small> <br/></div>' +
                        message.MessageBody + ' </p><hr/>'
                    $('#Inbox_Content').append(messageContent)
                })
                $('#Inbox_Content').append('<a class="text-center" href="/User/Notifications">Load More</a>')
            }
        });
    }
}


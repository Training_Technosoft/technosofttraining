﻿
document.getElementById("MessageOption").addEventListener("change", () => {
    document.getElementById("messageBox").style.display = "block"
    var option = document.getElementById("MessageOption").value
    if (option == "Role") {
        document.getElementById("RoleSelect").style.display = "block"
        document.getElementById("RoleMessageSend").style.display = "block"
        document.getElementById("UserSelect").style.display = "none"
        document.getElementById("UserMessageSend").style.display = "none"
    }
    else {
        document.getElementById("RoleSelect").style.display = "none"
        document.getElementById("RoleMessageSend").style.display = "none"
        document.getElementById("UserSelect").style.display = "block"
        document.getElementById("UserMessageSend").style.display = "block"
    }
    document.getElementById("Message").style.display = "block"   
});



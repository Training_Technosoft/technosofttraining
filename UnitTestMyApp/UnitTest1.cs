using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Training_Project;
using Training_Project.Controllers;
using Training_Project.Models;
using Training_Project.Repository;
using Moq;
using Training_Project.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace UnitTestMyApp
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public async System.Threading.Tasks.Task AddUser_DuplicateUser_RedirectToSameAction()
        {
            /*
            //Arange
            var userManager =  new Mock<UserManager<ApplicationUser>>(); 
            var mockRoleManager = GetMockRoleManager();
            var mockEmailSender = new Mock<IEmailSender>();
            var mockDepartmentRepository = new Mock<IDepartmentRepository>();
            AdminController controller = new AdminController(userManager.Object, mockRoleManager.Object,
                mockEmailSender.Object, mockDepartmentRepository.Object);
            UserInputViewModel user = new UserInputViewModel()
            {
                Email = "admin@techno-soft.com",
                UserName = "admin"
            };

            //Act
            var result =await controller.AddUser(user) as ViewResult;

            //Assert
            Assert.AreEqual("AddUser", result.ViewName);
            */
        }


        public static UserManager<TUser> TestUserManager<TUser>(IUserStore<TUser> store = null) where TUser : class
        {
            store = store ?? new Mock<IUserStore<TUser>>().Object;
            var options = new Mock<IOptions<IdentityOptions>>();
            var idOptions = new IdentityOptions();
            idOptions.Lockout.AllowedForNewUsers = false;
            options.Setup(o => o.Value).Returns(idOptions);
            var userValidators = new List<IUserValidator<TUser>>();
            var validator = new Mock<IUserValidator<TUser>>();
            userValidators.Add(validator.Object);
            var pwdValidators = new List<PasswordValidator<TUser>>();
            pwdValidators.Add(new PasswordValidator<TUser>());
            var userManager = new UserManager<TUser>(store, options.Object, new PasswordHasher<TUser>(),
                userValidators, pwdValidators, new UpperInvariantLookupNormalizer(),
                new IdentityErrorDescriber(), null,
                new Mock<ILogger<UserManager<TUser>>>().Object);
            validator.Setup(v => v.ValidateAsync(userManager, It.IsAny<TUser>()))
                .Returns(System.Threading.Tasks.Task.FromResult(IdentityResult.Success)).Verifiable();
            return userManager;
        }

        public static Mock<RoleManager<IdentityRole>> GetMockRoleManager()
        {
            var roleStore = new Mock<IRoleStore<IdentityRole>>();
            return new Mock<RoleManager<IdentityRole>>(
                         roleStore.Object, null, null, null, null);

        }
    }
}
